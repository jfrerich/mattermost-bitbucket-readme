# README #

Add this line for a PR Request
Add this line for a PR Request

This README describes the processes used to build and test the mattermost
bitbucket plugin

### Area for making comments to submist to repository
* this is for testing the webhook only
test 5

### Proposed Testing Methodology

* **DONE - Bitbucket Setup**
  * DONE - Webhook
  * DONE - Oath
* **Postman** 
  * Use Bitbucket API to perform the following on https://bitbucket.org/jfrerich/mattermost-bitbucket-readme/src/master/
    * Connect to mattermost-bitbucket-readme
    * POST repo details (TBD - owner, file list?? )
    * get notification for new push
* **server/** 
  * webhook.go
    * handleWebhook()
  * plugin.go
    * getOAuthConfig() - get Oath 
    * bitbucketConnect() - connect to BB server
  * api.go
    * ServeHTTP() - set paths to plugin.go
      * /webhook - webhook.handleWebhook()
      * /oauth/connect - connectUserToGitHub()
      * /oauth/complete - completeConnectUserToGitHub()
      * /api/v1/connected - p.getConnected()
* **webapp/**
  * create bitbucket user bot
  * connect bb to user account
  * create static components
    * icons 
  * /bitbucket slash command
    * /bb connect
    * /bb help
    * /bb subscribe bb.org/jfrerich/mattermost-bitbucket-readme
* **E2E testing**
  * connect 
  * /bb connect
  * notification sent from new push to bb.org/jfrerich/mattermost-bitbucket-readme


### Code Changes

** plugin.json **
  changing the following keys names required changes to to all locations in code
  
    GitHubOAuthClientSecret
    GitHubOAuthClientID

** server/plugin.go **
  func (p *Plugin) OnActivate() error {
    // print the config after activate.  checked keys are populated
    fmt.Printf("config = %+v\n", config)

Other NOTES on packages

** imports ** 
  * api.go
    * github.com/google/go-github/github
    * github.com/mattermost/mattermost-server/mlog
    * github.com/mattermost/mattermost-server/model
    * github.com/mattermost/mattermost-server/plugin
    * golang.org/x/oauth2
  
  * command.go
    * github.com/mattermost/mattermost-server/mlog
    * github.com/mattermost/mattermost-server/plugin
    * github.com/google/go-github/github
    * github.com/mattermost/mattermost-server/model
     
  * configuration.go
    * github.com/google/go-github/github
    * github.com/mattermost/mattermost-server/mlog
    * github.com/mattermost/mattermost-server/model
  
  * plugin.go
    * github.com/mattermost/mattermost-server/mlog
    * github.com/mattermost/mattermost-server/model
    * github.com/mattermost/mattermost-server/plugin
    * github.com/google/go-github/github
    * golang.org/x/oauth2
  
  * subscriptions.go
    * github.com/mattermost/mattermost-server/mlog
    * github.com/google/go-github/github
   
  * utils.go    
    
  * webhook.go    
    * github.com/google/go-github/github
    * github.com/mattermost/mattermost-server/mlog
    * github.com/mattermost/mattermost-server/model
     
  
** server/vendor/github.com/
  * mattermost/mattermost-server/plugin/ *
    * extend API to mattermmost server
    * 

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
